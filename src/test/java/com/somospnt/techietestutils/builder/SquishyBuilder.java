package com.somospnt.techietestutils.builder;

import com.somospnt.techietestutils.domain.Squishy;
import com.somospnt.test.builder.AbstractBuilder;

public class SquishyBuilder extends AbstractBuilder<Squishy> {

    private SquishyBuilder(Squishy squishy) {
        instance = squishy;
    }

    public static SquishyBuilder SquishyArcoiris() {
        Squishy squishy = new Squishy();
        squishy.setApretabilidad("muy apretable");
        squishy.setNombre("Kawaii");
        squishy.setPrecio("300");
        SquishyBuilder builder = new SquishyBuilder(squishy);
        return builder;
    }

    public SquishyBuilder conApretabilidad(String apretabilidad) {
        this.instance.setApretabilidad(apretabilidad);
        return this;
    }

    public SquishyBuilder conPrecio(String precio) {
        this.instance.setPrecio(precio);
        return this;
    }

    public SquishyBuilder conNombre(String nombre) {
        this.instance.setNombre(nombre);
        return this;
    }

}
