package com.somospnt.techietestutils.builder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somospnt.techietestutils.domain.Squishy;
import com.somospnt.techietestutils.service.SquishyServiceTest;
import com.somospnt.test.server.AbstractMockRestOperation;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.test.web.client.ResponseCreator;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestToUriTemplate;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

public class ObtenerSquishyMockOperation extends AbstractMockRestOperation {

    private String nombre;

    private ObtenerSquishyMockOperation(String nombre) {
        this.nombre = nombre;
    }

    public static ObtenerSquishyMockOperation conNombre(String nombre) {
        ObtenerSquishyMockOperation instance = new ObtenerSquishyMockOperation(nombre);
        instance.status = HttpStatus.ACCEPTED;
        return instance;
    }

    public static ObtenerSquishyMockOperation conSquishyInexistente(String nombre) {
        ObtenerSquishyMockOperation instance = new ObtenerSquishyMockOperation(nombre);
        instance.status = HttpStatus.NOT_FOUND;
        return instance;
    }

    @Override
    protected String getUrl() {
        return "http://localhost/q?nombre=" + nombre;
    }

    @Override
    protected HttpMethod getHttpMethod() {
        return HttpMethod.GET;
    }

    @Override
    protected ResponseCreator getSuccessResponse() {
        return withSuccess(getJsonRespuesta(), APPLICATION_JSON_UTF8);
    }

    @Override
    protected RequestMatcher getExpect() {
        return requestToUriTemplate(getUrl());
    }

    private String getJsonRespuesta() {
        Squishy squishiesResponse = SquishyBuilder.SquishyArcoiris().conNombre(nombre).build();

        String response = "";
        try {
            response = new ObjectMapper().writeValueAsString(squishiesResponse);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(SquishyServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
}
