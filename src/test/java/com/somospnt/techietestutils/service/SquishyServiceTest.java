package com.somospnt.techietestutils.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somospnt.techietestutils.TechieTestUtilsApplicationTests;
import com.somospnt.techietestutils.builder.ObtenerSquishyMockOperation;
import com.somospnt.techietestutils.builder.SquishyBuilder;
import com.somospnt.techietestutils.domain.Squishy;
import com.somospnt.test.server.MockRestServiceServerBuilder;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.client.RestTemplate;

public class SquishyServiceTest extends TechieTestUtilsApplicationTests {

    @Value("${com.techie.uri}")
    private String uri;
    @Autowired
    private SquishyService squishyService;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;
    private MockRestServiceServer mockRestServiceServer;

    @Before
    public void setUp() {
        mockRestServiceServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void obtenerPorNombres_conUnSoloNombreSinMockOperation_retornaUnSquishy() throws JsonProcessingException {

        Squishy squishyResponse = SquishyBuilder.SquishyArcoiris().build();
        String response = "";
        response = objectMapper.writeValueAsString(squishyResponse);

        mockRestServiceServer.expect(requestTo(uri + "/q?nombre=" + squishyResponse.getNombre()))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

        List<String> nombres = Arrays.asList(squishyResponse.getNombre());

        List<Squishy> squishies = squishyService.obtenerPorNombres(nombres);

        assertThat(squishies).isNotEmpty();

        mockRestServiceServer.verify();
    }

    @Test
    public void obtenerPorNombres_conUnSoloNombreConMockOperation_retornaUnSquishy() {
        List<String> nombres = Arrays.asList("Kawaii");
        MockRestServiceServer mockRestServiceServer = ObtenerSquishyMockOperation
                .conNombre(nombres.get(0))
                .mock(restTemplate);

        List<Squishy> squishies = squishyService.obtenerPorNombres(nombres);

        assertThat(squishies.size()).isEqualTo(1);

        mockRestServiceServer.verify();
    }

    @Test
    public void obtenerPorNombres_conMockOperation_retornaListaDeSquishys() {
        List<String> nombres = Arrays.asList("Kawaii", "NyamCat", "ASD");
        MockRestServiceServer mockRestServiceServer = MockRestServiceServerBuilder
                .withRestTemplate(restTemplate)
                .withMockOperation(ObtenerSquishyMockOperation.conNombre(nombres.get(0)))
                .withMockOperation(ObtenerSquishyMockOperation.conNombre(nombres.get(1)))
                .withMockOperation(ObtenerSquishyMockOperation.conNombre(nombres.get(2)))
                .build();

        List<Squishy> squishies = squishyService.obtenerPorNombres(nombres);

        assertThat(squishies.size()).isEqualTo(3);

        mockRestServiceServer.verify();
    }

    @Test
    public void obtenerPorNombres_conMockOperationDiferentes_retornaListaDeSquishys() {
        List<String> nombres = Arrays.asList("Kawaii", "NyamCat", "ASD");

        MockRestServiceServer mockRestServiceServer = MockRestServiceServerBuilder
                .withRestTemplate(restTemplate)
                .withMockOperation(ObtenerSquishyMockOperation.conNombre(nombres.get(0)))
                .withMockOperation(ObtenerSquishyMockOperation.conNombre(nombres.get(1)))
                .withMockOperation(ObtenerSquishyMockOperation.conSquishyInexistente(nombres.get(2)))
                .build();

        List<Squishy> squishies = squishyService.obtenerPorNombres(nombres);

        assertThat(squishies.size()).isEqualTo(2);

        mockRestServiceServer.verify();
    }

}
